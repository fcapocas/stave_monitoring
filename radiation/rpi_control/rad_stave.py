import time
from sht85 import sht85
import os
from influx import InfluxDB
from HH import HH309A


client = InfluxDB('http://10.2.237.10:8086')

Star11=sht85(1) #black

Star14=sht85(5) #red
sensor=HH309A("/dev/ttyUSB0")
        
while True:
    try:
        data1=Star11.get_data()
        data2=Star14.get_data()
        
        client.write('rad_stave', 'humidity chiller end' , fields={'humidity': data1['humidity']})
        client.write('rad_stave', 'temperature chiller end', fields={'temperature': data1['temperature']})

        client.write('rad_stave','humidity computer end', fields={'humidity': data2['humidity']})
        client.write('rad_stave','temperature computer end', fields={'temperature': data2['temperature']})
        print(data1)
        print(data2)
        therm_data= sensor.get_data()
        print(therm_data)
        client.write('rad_stave','Thermocouple 1  chiller end', fields={'temperature': therm_data['t1']})
        client.write('rad_stave', 'Thermocouple 2 computer end', fields={'temperature': therm_data['t2']})
                     
        print("sending data")
    except IOError as e:
        print(e)
    time.sleep(5)
