# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Confirmation2.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Confirmation2(object):
    def setupUi(self, Confirmation2):
        Confirmation2.setObjectName("Confirmation2")
        Confirmation2.resize(339, 178)
        self.Label = QtWidgets.QLabel(Confirmation2)
        self.Label.setGeometry(QtCore.QRect(100, 30, 161, 41))
        self.Label.setObjectName("Label")
       
        self.retranslateUi(Confirmation2)
        QtCore.QMetaObject.connectSlotsByName(Confirmation2)

    def retranslateUi(self, Confirmation2):
        _translate = QtCore.QCoreApplication.translate
        Confirmation2.setWindowTitle(_translate("Confirmation2", "Form"))
        self.Label.setText(_translate("Confirmation2", "Is the Booster Pump OFF?"))
     #  self.TurnedON.setText(_translate("Confirmation2", "YES!"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Confirmation2 = QtWidgets.QWidget()
    ui = Ui_Confirmation2()
    ui.setupUi(Confirmation2)
    Confirmation2.show()
    sys.exit(app.exec_())
