import serial
from serial import *
import time

from influx import InfluxDB
client = InfluxDB('http://127.0.0.1:8086')
   

class SPS:
    def __init__(self,location="/dev/ttyUSB0"):
        self.chiller=serial.Serial(location,9600,SEVENBITS,parity=PARITY_NONE,stopbits=STOPBITS_ONE,timeout=10)
        
    def SetRampRate(self,rate):
        query="RR="+str(rate)+"\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
        
    def SetTemperature(self,temp):
        query="SP="+str(temp)+"\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
        
    def TurnOn(self):
        query="START\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
    
    def TurnOff(self):
        query="STOP\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
    
    def GetTemperature(self, remote=False):
        if remote:
            query="PTREM?\n\r"
        else:
            query="PTLOC?\n\r"
        self.chiller.write(query.encode())
        data=self.chiller.readline()
        print(data)
        return float(data[data.index("=")+1:data.index("!")])

    def GetRampRate(self):
        query="RR?\n\r"
        self.chiller.write(query.encode())
        data=self.chiller.readline()
        print(data)
        return float(data[data.index("=")+1:data.index("!")])

    def SwitchSensor(self,remote):
        if remote !=0 and remote !=1:
            print("0 for internal 1 for remote")
            return
        else:
            query="CTLREM="+str(remote)+"\n\r"
            self.chiller.write(query.encode())
            data=self.chiller.readline()
            print(data)

def setup(chiller):
    chiller.TurnOn()
    chiller.SwitchSensor(1)
if __name__=="__main__":
    chiller=SPS()
   #setup(chiller)
   #chiller.SetTemperature(25)
    chiller.TurnOff()
    while True:
        try:
            fra=client.write('rad_stave', 'chiller_internal_temp', fields={'temp': chiller.GetTemperature(remote=False)})
            print(fra)
            client.write('rad_stave', 'chiller_remote_temp', fields={'temp': chiller.GetTemperature(remote=True)})
            time.sleep(5)
        except IOError:
            continue
    
            
