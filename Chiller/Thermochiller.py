# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Thermochiller.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from Confirmation1 import Ui_Confirmation1
from Confirmation2 import Ui_Confirmation2
import serial
from serial import *
import time
import threading

#from influxdb import influxdb
from influx import InfluxDB
client = InfluxDB('http://127.0.0.1:8086')

lock=threading.Lock()


class SPS():
    def __init__(self,location="/dev/ttyUSB0"):
        self.chiller=serial.Serial(location,9600,SEVENBITS,parity=PARITY_NONE,stopbits=STOPBITS_ONE,timeout=10)
        
    def SetRampRate(self,rate):
        query="RR="+str(rate)+"\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
        
    def SetTemperature(self,temp):
        with lock:
            query="SP="+str(temp)+"\n\r"
            self.chiller.write(query.encode())
            print(self.chiller.readline())
        
    def TurnOn(self):
        query="START\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
    
    def TurnOff(self):
        with lock:
            query="STOP\n\r"
            self.chiller.write(query.encode())
            print(self.chiller.readline())
    
    def GetTemperature(self, remote=False):
        if remote:
            query="PTREM?\n\r"
        else:
            query="PTLOC?\n\r"
        self.chiller.write(query.encode())
        data=self.chiller.readline()
        print(data)
        data=data.decode()
        return float(data[data.index("=")+1:data.index("!")])

    def GetRampRate(self):
        query="RR?\n\r"
        self.chiller.write(query.encode())
        data=self.chiller.readline()
        print(data)
        return float(data[data.index("=")+1:data.index("!")])

    def SwitchSensor(self,remote):
        if remote !=0 and remote !=1:
            print("0 for internal 1 for remote")
            return
        else:
            query="CTLREM="+str(remote)+"\n\r"
            self.chiller.write(query.encode())
            data=self.chiller.readline()
            print(data)
    def setup(chiller):
        with lock:
            chiller.TurnOn()
            chiller.SwitchSensor(1)

class Ui_ThermochillerGUI(object):
    
    
    def onclickedOFF(self):
        self.window = QtWidgets.QWidget()
        self.ui = Ui_Confirmation2()
        self.ui.setupUi(self.window)
        self.window.show()
    
    def onclickedON(self):
        self.window = QtWidgets.QWidget()
        self.ui = Ui_Confirmation1()
        self.ui.setupUi(self.window)
        self.window.show()
    
    def setup(chiller):
        with lock:
            chiller.TurnOn()
            chiller.SwitchSensor(1)
    
    
    
    
    
    
    
    
    def setupUi(self, ThermochillerGUI):
        chiller=SPS()
        ThermochillerGUI.setObjectName("ThermochillerGUI")
        ThermochillerGUI.resize(421, 326)
        ThermochillerGUI.setAutoFillBackground(True)
        self.centralwidget = QtWidgets.QWidget(ThermochillerGUI)
        self.centralwidget.setObjectName("centralwidget")
        self.spinBox = QtWidgets.QSpinBox(self.centralwidget)
        self.spinBox.setGeometry(QtCore.QRect(40, 210, 151, 71))
        self.spinBox.setMinimum(-70)
        self.spinBox.setMaximum(60)
        self.spinBox.setProperty("value", 20)
        self.spinBox.setObjectName("spinBox")
        
        self.SetTemp = QtWidgets.QPushButton(self.centralwidget)
        self.SetTemp.setGeometry(QtCore.QRect(250, 210, 121, 71))
        self.SetTemp.setObjectName("SetTemp")
        
        
        self.OFFButton = QtWidgets.QPushButton(self.centralwidget)
        self.OFFButton.setGeometry(QtCore.QRect(50, 30, 111, 81))
        self.OFFButton.setObjectName("OFFButton")
        
        self.ONButton = QtWidgets.QPushButton(self.centralwidget)
        self.ONButton.setGeometry(QtCore.QRect(250, 30, 111, 81))
        self.ONButton.setObjectName("ONButton")
        
        
        
        ThermochillerGUI.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(ThermochillerGUI)
        self.statusbar.setObjectName("statusbar")
        ThermochillerGUI.setStatusBar(self.statusbar)

        self.retranslateUi(ThermochillerGUI)
        QtCore.QMetaObject.connectSlotsByName(ThermochillerGUI)
        
        
        chiller.TurnOff()
        
        self.ONButton.clicked.connect(self.onclickedOFF)
        self.ONButton.clicked.connect(chiller.TurnOff)
        self.OFFButton.clicked.connect(self.onclickedON)
        self.OFFButton.clicked.connect(lambda: SPS.setup(chiller))
        
        self.SetTemp.clicked.connect(lambda: chiller.SetTemperature(self.spinBox.value()))
        
        

    def retranslateUi(self, ThermochillerGUI):
        _translate = QtCore.QCoreApplication.translate
        ThermochillerGUI.setWindowTitle(_translate("ThermochillerGUI", "MainWindow"))
        self.SetTemp.setText(_translate("ThermochillerGUI", "SetTemp"))
        self.OFFButton.setText(_translate("ThermochillerGUI", "ON"))
        self.ONButton.setText(_translate("ThermochillerGUI", "OFF"))



def read_chiller(chiller):
  while True:
        try:
            with lock:
                #dbClient.write_points(loginEvents)
                client.write('rad_stave', 'chiller_internal_temp',fields={'temp': chiller.GetTemperature(remote=False)})
                #client.write('rad_stave', 'chiller_remote_temp', fields={'temp': chiller.GetTemperature(remote=True)})
                client.write('rad_stave', 'chiller_remote_temp', fields={'temp': chiller.GetTemperature(remote=True)})
            time.sleep(5)
        except IOError:
            print("Encountered error")
            time.sleep(1)
            continue 

if __name__ == "__main__":
    import sys
    chiller=SPS()

    app = QtWidgets.QApplication(sys.argv)
    ThermochillerGUI = QtWidgets.QMainWindow()
    ui = Ui_ThermochillerGUI()
    ui.setupUi(ThermochillerGUI)
    ThermochillerGUI.show()
    query= threading.Thread(target=read_chiller, args=(chiller,))
    query.start()
 
    
    sys.exit(app.exec_())
