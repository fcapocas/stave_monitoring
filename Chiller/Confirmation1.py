# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Confirmation1.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Confirmation1(object):
    def setupUi(self, Confirmation1):
        Confirmation1.setObjectName("Confirmation1")
        Confirmation1.resize(339, 178)
        self.Label = QtWidgets.QLabel(Confirmation1)
        self.Label.setGeometry(QtCore.QRect(100, 30, 161, 41))
        self.Label.setObjectName("Label")
        

        self.retranslateUi(Confirmation1)
        QtCore.QMetaObject.connectSlotsByName(Confirmation1)

    def retranslateUi(self, Confirmation1):
        _translate = QtCore.QCoreApplication.translate
        Confirmation1.setWindowTitle(_translate("Confirmation1", "Form"))
        self.Label.setText(_translate("Confirmation1", "Turn the Booster Pump ON"))
#        self.TurnedON.setText(_translate("Confirmation1", "Turned ON"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Confirmation1 = QtWidgets.QWidget()
    ui = Ui_Confirmation1()
    ui.setupUi(Confirmation1)
    Confirmation1.show()
    sys.exit(app.exec_())
